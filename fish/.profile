#!/bin/zsh

# zsh profile file. Runs on login. Environmental variables are set here.

# If you don't plan on reverting to bash, you can remove the link in ~/.profile
# to clean up.

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"

#SHELL=zsh

# Default programs:
export ALTERNATE_EDITOR=""
export EDITOR="emacsclient -c"                  # $EDITOR opens in terminal
export VISUAL="emacsclient -c -a emacs"         # $VISUAL opens in GUI mode
export TERMINAL="alacritty"
export BROWSER="zen-browser"
export READER="zathura"

# -#- XDG Base Directory -#- {{{1
# BASIC {{{2
export XDG_LOCAL_HOME="$HOME/.local" # This sould be one of defaults

export XDG_CACHE_HOME="$HOME/.local/cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_DIR="$HOME/.config"
export XDG_DATA_HOME="$XDG_LOCAL_HOME/share"

# CUSTOM {{{2
# export _XDG_WRAPPERS="$XDG_LOCAL_HOME/bin/_xdg_wrappers"
export XDG_FAKEHOME_DIR="$XDG_LOCAL_HOME/data"
export XDG_GAMES_DIR="$XDG_LOCAL_HOME/games"
export XDG_HISTORY_DIR="$XDG_CACHE_HOME/history"
export XDG_VM_DIR="$XDG_DATA_HOME/vm"

# PATH {{{2
# export PATH="$XDG_LOCAL_HOME/AppImages:$PATH"
export PATH="$XDG_LOCAL_HOME/bin:$PATH"
export PATH="$XDG_LOCAL_HOME/scripts:$PATH"
export PATH="$XDG_LOCAL_HOME/bin/wrappers:$PATH"

# RELOCATING VARIABLES {{{2
# export XINITRC="/home/me/.config/xinitrc" # NOT WORKING

export BASH_HISTORY_USER_FILE="$XDG_CONFIG_HOME/bash/bash_history"
export BASH_COMPLETION_USER_FILE="$XDG_CONFIG_HOME/bash/bash_completion"
export BASH_DOTDIR="$XDG_CONFIG_HOME/bash"
export ELECTRUMDIR="$XDG_DATA_HOME/electrum"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"
# export ENV="$XDG_CONFIG_HOME/shell/shrc"  # sh, ksh
export GIT_TEMPLATE_DIR="$XDG_CONFIG_HOME/git/template"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GOPATH="$XDG_DATA_HOME/go"
export GOPS_CONFIG_DIR="$XDG_CONFIG_HOME/gops" # temporary
export GRIPHOME="$XDG_CONFIG_HOME/grip"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export ICEAUTHORITY="$XDG_CACHE_HOME/ICEauthority"
export IMAPFILTER_HOME="$XDG_CONFIG_HOME/imapfilter"
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"
export MAILCAP="$XDG_CONFIG_HOME/mailcap" # elinks, w3m
export MAILCAPS="$MAILCAP"   # Mutt, pine
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/config.py"
export PYTHONUSERBASE="$XDG_LOCAL_HOME"
export RANDFILE="$XDG_CACHE_HOME/rnd"
export TEXMFHOME="$XDG_LOCAL_HOME/texmf"
export VIMDOTDIR="$XDG_CONFIG_HOME/vim"
export VIMINIT='set rtp^=$VIMDOTDIR | let $MYVIMRC="$VIMDOTDIR/vimrc" | so $MYVIMRC'
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc" #Also update the file /usr/share/alsa/alsa.conf with "$XDG_CONFIG_HOME/alsa/asoundrc"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export PULSE_COOKIE="$XDG_CONFIG_HOME/pulse/cookie"
export __GL_SHADER_DISK_CACHE_PATH="$XDG_CACHE_HOME/nv"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"

# HISTORY FILES {{{3
export HISTFILE="$XDG_HISTORY_DIR/shell"
export LESSHISTFILE=-
export MYSQL_HISTFILE="$XDG_HISTORY_DIR/mysql"
export NODE_REPL_HISTORY="$XDG_HISTORY_HOME/node_repl_history"
export SQLITE_HISTORY="$XDG_HISTORY_DIR/sqlite"

# Other program settings:
export DICS="/usr/share/stardict/dic/"
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"
export QT_QPA_PLATFORMTHEME="gtk2"	# Have QT use gtk2 theme.
export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.

# For GIMP 2.99 and GimpFu v3
# export PYTHONPATH="$XDG_CONFIG_HOME/GIMP/2.99/plug-ins/gimpfu"

export SHELL="$(command -v $SHELL)"

# Create history dir
mkdir -p "$XDG_HISTORY_DIR"

# just in case
[ "$XDG_RUNTIME_DIR" = "$XDG_LOCAL_HOME/.runtime" ] && mkdir -p "$XDG_RUNTIME_DIR"

# Start graphical server on tty1 if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! ps -e | grep -qw Xorg && exec hyprland
#[ "$(tty)" = "/dev/tty1" ] && ! ps -e | grep -qw Xorg && exec startx

# Switch escape and caps if tty and no passwd required:
# sudo -n loadkeys $XDG_DATA_HOME/larbs/ttymaps.kmap 2>/dev/null
