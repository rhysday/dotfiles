{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.KEY_ENTER)"
        }, 
        "B": {
            "action": "button(Keys.KEY_ESC)"
        }, 
        "BACK": {
            "action": "button(Keys.KEY_LEFTMETA) and button(Keys.KEY_Q)"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "CPADPRESS": {
            "action": "button(Keys.BTN_MOUSE)"
        }, 
        "LB": {
            "action": "button(Keys.KEY_LEFTMETA)"
        }, 
        "LGRIP": {
            "action": "button(Keys.BTN_SIDE)"
        }, 
        "LPAD": {
            "action": "button(Keys.KEY_LEFTMETA) and button(Keys.KEY_F)"
        }, 
        "RB": {
            "action": "keyboard()"
        }, 
        "RGRIP": {
            "action": "button(Keys.BTN_EXTRA)"
        }, 
        "RPAD": {
            "action": "button(Keys.BTN_MIDDLE)"
        }, 
        "START": {
            "action": "button(Keys.KEY_LEFTMETA) and button(Keys.KEY_G)"
        }, 
        "STICKPRESS": {
            "action": "button(Keys.KEY_LEFTCTRL)"
        }, 
        "X": {
            "action": "repeat(button(Keys.KEY_LEFTSHIFT))"
        }, 
        "Y": {
            "action": "button(Keys.KEY_TAB)"
        }
    }, 
    "cpad": {
        "action": "mouse()"
    }, 
    "dpad": {}, 
    "gyro": {}, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "sens(1.0, -1.0, XY(None, mouse(Rels.REL_WHEEL, 1)))"
    }, 
    "pad_right": {
        "action": "smooth(8, 0.78, 2.0, sens(2.0, 2.0, ball(mouse())))"
    }, 
    "rstick": {}, 
    "stick": {
        "action": "dpad(button(Keys.KEY_LEFTSHIFT) and button(Keys.KEY_EQUAL), button(Keys.KEY_MINUS), button(Keys.KEY_LEFT), button(Keys.KEY_RIGHT))"
    }, 
    "trigger_left": {
        "action": "trigger(51, 255, button(Keys.BTN_RIGHT))"
    }, 
    "trigger_right": {
        "action": "trigger(50, 255, button(Keys.BTN_MOUSE))"
    }, 
    "version": 1.4
}